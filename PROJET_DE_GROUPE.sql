create database librairie;
use librairie;

create table adresse (
idAdresse INT NOT NULL PRIMARY KEY,
n°Voie SMALLINT NOT NULL,
typeVoie varchar(10) NOT NULL,
nomVoie varchar(30) NOT NULL,
ville varchar(20) NOT NULL,
codePostal char(5) NOT NULL,
pays varchar(15) NOT NULL
);

create table ouvrage (
idOuvrage INT NOT NULL PRIMARY KEY,
ISBN INT NOT NULL,
auteur varchar(15) NOT NULL,
titre varchar(15) NOT NULL,
sousTitre varchar(20) NOT NULL,
editeur varchar(15) NOT NULL,
prix SMALLINT NOT NULL,
resumee varchar(150) NOT NULL,
imageCouverture BLOB NOT NULL
);

create table clients (
idClient INT NOT NULL PRIMARY KEY,
nomC varchar(15) NOT NULL,
prenomC varchar(15) NOT NULL
);

create table commande (
idCommande INT NOT NULL PRIMARY KEY,
datee DATE,
idAdresse INT NOT NULL,
idClient INT NOT NULL,
 FOREIGN KEY (idAdresse) REFERENCES adresse(idAdresse),
    FOREIGN KEY ( idClient) REFERENCES clients(idClient)
);

create table rubrique (
idRubrique INT NOT NULL PRIMARY KEY,
nomRubrique varchar(15) NOT NULL
);

 create table possede (
 idRubrique INT NOT NULL PRIMARY KEY,
 idOuvrage INT NOT NULL,
  FOREIGN KEY (idOuvrage) REFERENCES ouvrage(idOuvrage),
     FOREIGN KEY ( idRubrique) REFERENCES rubrique(idRubrique)
 );

create table ligneCommande (
idLigneCommande INT NOT NULL PRIMARY KEY,
qté SMALLINT NOT NULL,
sousTotal SMALLINT NOT NULL,
idCommande INT NOT NULL,
idOuvrage INT NOT NULL,
 FOREIGN KEY (idOuvrage) REFERENCES ouvrage(idOuvrage),
    FOREIGN KEY ( idCommande) REFERENCES commande(idCommande)
);

CREATE TABLE themes (
    idThemes INT NOT NULL primary key,
    nomThemes VARCHAR(10)
);

create table appartient (
 idOuvrage INT NOT NULL ,
 idThemes INT NOT NULL,
 FOREIGN KEY (idOuvrage) REFERENCES ouvrage(idOuvrage),
 FOREIGN KEY (idThemes) REFERENCES themes(idThemes)
 ); 
 